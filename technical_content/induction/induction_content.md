# THG Technology and Innovation Accelerator Induction

## Induction Schedule

### Day 1

**09:00**     Arrival and passport checks

**09:30**     Ice Breaker 

**10:00**     Graduate Talent Welcome and THG Accelerator Welcome 

**10:30**     Senior Welcome

**11:00**     Break and Laptops 

**11:30**     IT Security

**12:00**     Divisional Overview – Technology

**12:30**     Networking Lunch

**13:15**     Divisional Overview – Core Beauty

**13:45**     Divisional Overview – THG Brands

**14:15**     Break and Laptops

**14:45**     Impact, Influence and Success at THG

**15:15**     Break and Laptops

**15:45**     Divisional Overview - Ingenuity

**16:15**     Divisional Overview – Consumer Lifestyle

**17:15**     Wrap Up/Close


-------------------------------------

### Day 2

**09:00**     Welcome to Day 2 

**09:10**     Health and Safety Overview

**09:40**     Welcome – Technology Talent Team 

**09:50**     The Structure of Tech at THG  

**10:20**     Project Management at THG 

**10:50**     Data Engineering   

**11:20**     Break

**11:50**     Cyber and Information Security 

**12:20**     Lunch    

**13:00**     Infrastructure 

**13:30**     The Customer Journey 

**14:00**     Loyalty  

**14:30**     Data Science 

**15:00**     Break

**15:30**     Tech Services and Third Party  

**16:00**     THG Graduate Technologist panel

**17:15**     Close 
