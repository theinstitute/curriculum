# Technical Curriculum 

## Term 1

- [THG Accelerator Induction](induction.md)

- [Week 1]() (Wednesday to Friday)

- [Week 2]()

- [Week 3]()

- [Week 4]()

- [Week 5]()

- [Week 6]()

- [Week 7]()

- [Week 8]()

- [Week 9]()

- [Week 10]()

- [Week 11]()

- [Week 12]()

- [Week 13]()

- [Week 14]()

- [Week 15]()

-------------------------------------------

## Term 2

- [Week 16]()

- [Week 17]()

- [Week 18]()

- [Week 19]()

- [Week 20]()

- [Week 21]()

- [Week 22]()

- [Week 23]()

- [Week 24]()

- [Week 25]()

- [Week 26]()


----------------------------

## Supporting Materials
Here you will find other, relevant materials and challenges that will help you on your quest of becoming a software developer in a matter of weeks!

[Daily schedule]() - an outline of a standard daily schedule

[The Escalation Process]() - our expected escalation process

[Challenges]() - additional challenges for each week 

[Knowledge Bytes]() - useful terms and references