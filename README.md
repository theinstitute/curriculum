# THG Technology and Innovation Accelerator

Welcome to THG Accelerator!

Over the course of the next 6 months, you will be laying the foundations for your technology career. 

Each week, we will introduce you to new concepts and encourage you to apply your new knowledge and skills with different projects and coding exercises.



Your goal, as a Graduate Software Enginner through our Accelerator programme, is not just to learn how to build software. You must learn how to build it in the right way, following the best industry practices.

We have no rigid timetable here at THG Accelerator. We have designed the learning day to reflect similar structures and processes to that of a software engineering team. Timetables and scheduled activities are therefore flexible and subject to change. Any change or sheduled activity will be communicated to you all in advance via Hipchat. A sample schedule can be found [here](schedule.md).  


Are you ready? Go to the [Course outline!](course_outline.md)