# Industry Primer

This course of content is designed to provide you with the necessary context to the technical skills you are acquiring. At then end of the course, we hope that you will have an understanding of e-commerce and the role that technology plays in each area of our business. This course content will be integrated across all of your 6 month Accelerator programme.  

![alt text](img/primer.png)

---------------------------

## Resources 

_A list of links to useful resources to accompany each of your industry primer modules:_



- [E-commerce]()
- [Marketing]()_Ads (CPC, PPC, social, photo/video platforms) SEO, eCRM, affiliates._
- [Life of an order]() _From browse to delivery via logistics._
- [Life of a customer]() _Journey of a Customer from first contact to repeat purchase._
- [Life of a product]() _From NPD (new product development) to appearing on a website, purchase, supply chain, through to logistics._
- [Customer insight]() _Customer behaviour and Customer value._
- [Global considerations]() _Internationalisation, localisation, shipping laws, currency, languages, culture, time of day, product offering and holidays._
- [Financial considerations]() _Cost of development, benefit, ownership, building in-house, outsourcing, cost of sales, fluctuations in supplied goods._