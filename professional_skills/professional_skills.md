# Skills for the Professional Software Engineer

To function effectively, software engineers do not only require the appropriate technical skills, knowledge and experience, but also an understanding of the context in which they work. Professional software engineers should understand the repationship between technological change, society and the law, and the powerful role that computers and those working with them play in society. This area of study serves as an introduction to developing a critical understanding of the Legal, Social, Ethical and Professional Issues (LSEPI) that are material to the Technology industry.

This course is designed to support graduates’ professional development at the THG Accelerator. Through the programme participants will develop key skills, learn powerful personal development principles and gain exposure to wider issues of importance in the technology sector.

By the end of the programme all participants will have a secure understanding of the fundamentals of professional development, be equipped to access Management and Leadership Development concepts in the future, as well as maintain their development autonomously.


## Course Structure and Content

The course is broadly made up of 20 hour-long sessions. These can be broken down into 3 core strands@ 

[Professional Development](professional_development.md)

_These sessions develop the mantle of professional understanding  for THG graduates._

[Understanding THG](understanding_THG.md)

_Understanding THG elements are designed to broaden and deepen graduates understanding of THG as an industry-leading business, and our diverse roles within it._

[An introduction to...](introduction.md)

_A brief outline of issues in technology today._


------------------------------------------

