# Introduction to ...

## Ethics

- The Ancient Art
- Modern Applications of Ethics
- Digital Dilemmas

## Privacy and Data Protection

- Impact of privacy and data laws
- GDPR
- Business Risk
- Best Practice
- Issues of Morality/Confidentiality

## Project Management

- Core Principles
- Defining Goals, Planning
- Project Governance
- Waterfall / SCRUM / Agile working
- Reporting progress
- Risk, accountability
- Evaluation, feedback

## Unconscious bias

- Coding for discrimination
- How biases translate into code
- Real world impact
- Best practice
