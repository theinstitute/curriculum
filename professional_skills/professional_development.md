# Professional Development

**Building Relationships in a New Role**
 
- The importance of first impressions
- Relationship with manager
- Relationship with colleagues
- Best practice professionalism 
- Your reputation


**Self-Reflection**

- Being a reflective professional
- How reflection drives improvement
- How reflection guards wellbeing
- Reflective practices


**Workplace Essentials**

- Professionalism 
- Being a role model
- Approachability and visibility
- Communication and confidentiality
- Best practice on e-mail
- Meeting etiquette
- Dealing with pressure and self-care


**Collaboration**

- Models of leadership
- The importance of collaboration
- At THG
- Risks
- Distributed leadership and collaboration for competition


**Taking initiative**

- What is initiative?
- What does it look like?
- Circle of influence/ circle of concern
- Benefits of initiative
- Responsible initiative
- Scenario work


**Influence**

- Influence as a social language
- Influence as power
- A model of influence (golden circle)
- Using influence to inspire others
- Being authentically influential
- Using influence to inspire yourself


**Time Management**

- Principles of time management
- Modern time management challenges
- Pomodoro technique
- Routine (what works for you?)
- Accountability
- Outlook


**Interview Skills**

- Internal Opportunities
- Sideways / upwards moves
- Best practice
- What the interviewer expects
- Top tip, practice


**Harnessing Strengths and Weaknesses**
- Strength Finder
- Growth Mindset
- Harpening Strengths
- Shaping Weaknesses
- Putting into practice


**Defining Values**

- The importance of values
- Benefits
- "in flow"
- Identitying values
- Cost
- Vision


**Presentation Skills**

- What makes a presentation?
- Preparation
- Professional persona
- 'Live' aids
- Take homes


**Continued Development**

- What does it mean? 
- Benefits of autonomy
- Expectations: revisiting, experimenting, growing
- Target setting
- Support available