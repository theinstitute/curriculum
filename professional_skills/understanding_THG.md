# Understanding THG

## Understanding THG: Brand Values

- Exploration of THG Values: Innovation, Ambition, Decisiveness
- Why values? 
- The literature
- In practice

## Understanding THG: Our Brands

- Ambassadors from THG brands introduce: 
    - their role
    - brand indentities, successes,
    - products and USPs, 
    - psition in the market and plans for the future
    
## Understanding THG: Omega

- Tour of our Omega warehouse
- Introduction to its capabilities
- Its structure and organisation
- How technology underpins our work at Omega

## Understanding THG: Trading and Marketing

- Ambassador talk
- Marketing and trading importance to THG
- Modern Practices and Systems
- PPC and The Creative Team 