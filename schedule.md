# A day in the life


**9.00 a.m.** - Arrival: * We will start to get prepped for the day ahead.*

**9.30 a.m.** - Stand up: *We will have three stand up groups and each group will have a designated scribe and a facilitator. The Facilitator leads the stand up with each participant taking turns to state their answers to the three questions:* 

1) What have I learnt since the last stand up? 
2) What do I plan to learn before the next stand up? 
3) What have I struggled with since the last stand up) 


*The scribe is responsible for recording these and sharing their brief notes with the other stand up groups on hipchat.*

(You can find out more about stand-ups [here](stand-ups.md))

**10 a.m.** - Lecture/Workshop: _lectures and workshops will be used to introduce new concepts_

**11 a.m** - Hands-on: _practical work based on the application of new concepts._

**12:45 p.m.** – Lunch: *Time to refuel!*

**13:30 p.m.** - Hands-on: _continuing with practical work._

**15:00 p.m.** - Breakout session/Lecture: _if required. To clarify issues and problems encoutered during the Hands-on or to introduce more new concepts._

**17:00 p.m.** - Wash up: _summary of the days learnings and information about the following day_




(_On Friday’s at 16.30 a retrospective will be conducted. You can find out more about retrospectives and their purpose_ [here](retros.md))
